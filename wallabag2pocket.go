package wallabag2pocket

import (
	"encoding/xml"
)

type WallabagArticle struct {
	Tags []string `json:"tags"`
	Url  string   `json:"url"`
}

type DescriptionList struct {
	XMLName xml.Name `xml:"DL"`
	Terms   []DescriptionTerm
}

type DescriptionTerm struct {
	XMLName      xml.Name `xml:"DT"`
	Header       string   `xml:"H3,omitempty"`
	Link         *Anchor  `xml:"A"`
	Descriptions *DescriptionList
}

type Anchor struct {
	Href string `xml:"href,attr"`
}
