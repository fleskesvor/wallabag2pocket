package main

import (
	"encoding/json"
	"encoding/xml"
	"flag"
	. "fleskesvor/wallabag2pocket"
	"fmt"
	"io/ioutil"
	"os"
)

const Doctype = "<!DOCTYPE NETSCAPE-Bookmark-file-1>"

func main() {
	articleFile := flag.String("i", "", "article file to import from")
	bookmarkFile := flag.String("e", "", "bookmark file to export to")
	flag.Parse()

	if *articleFile == "" || *bookmarkFile == "" {
		flag.Usage()
		return
	}

	articles, err := readArticleFile(*articleFile)

	if err != nil {
		fmt.Println("Error reading file: ", err)
		return
	}

	bookmarkList := createStructuredBookmarks(articles)
	err = writeBookmarkFile(bookmarkList, *bookmarkFile)

	if err != nil {
		fmt.Println("Error writing file: ", err)
		return
	}
}

func createStructuredBookmarks(articles []WallabagArticle) DescriptionList {
	articlesWithTags, articlesWithoutTags := groupArticlesWithAndWithoutTags(articles)
	var bookmarks = createBookmarks(articlesWithoutTags)

	for tag, hrefs := range articlesWithTags {
		bookmarks = append(bookmarks, DescriptionTerm{
			Header:       tag,
			Descriptions: &DescriptionList{Terms: createBookmarks(hrefs)},
		})
	}

	return DescriptionList{Terms: bookmarks}
}

func groupArticlesWithAndWithoutTags(articles []WallabagArticle) (map[string][]string, []string) {
	articlesWithTags := make(map[string][]string)
	articlesWithoutTags := make([]string, 0)

	for _, article := range articles {
		if len(article.Tags) == 0 {
			articlesWithoutTags = append(articlesWithoutTags, article.Url)
		}
		for _, tag := range article.Tags {
			if _, exists := articlesWithTags[tag]; !exists {
				articlesWithTags[tag] = make([]string, 0)
			}
			articlesWithTags[tag] = append(articlesWithTags[tag], article.Url)
		}
	}

	return articlesWithTags, articlesWithoutTags
}

func createBookmarks(hrefs []string) []DescriptionTerm {
	var bookmarks = make([]DescriptionTerm, 0)

	for _, href := range hrefs {
		bookmarks = append(bookmarks,
			DescriptionTerm{
				Link: &Anchor{Href: href},
			})
	}

	return bookmarks
}

func readArticleFile(filename string) ([]WallabagArticle, error) {
	file, _ := ioutil.ReadFile(filename)
	var articles []WallabagArticle

	err := json.Unmarshal(file, &articles)

	return articles, err
}

func writeBookmarkFile(rootElement DescriptionList, filename string) error {
	bookmarkFile, err := os.Create(filename)

	if err != nil {
		return err
	}

	_, err = bookmarkFile.WriteString(Doctype + "\n")

	if err != nil {
		return err
	}

	encoder := xml.NewEncoder(bookmarkFile)
	encoder.Indent("", "\t")
	err = encoder.Encode(&rootElement)

	if err != nil {
		return err
	}

	return nil
}
