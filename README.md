# Wallabag 2 Pocket

Import Wallabag articles with tags into Pocket using the
[Netscape Bookmark File Format](https://learn.microsoft.com/en-us/previous-versions/windows/internet-explorer/ie-developer/platform-apis/aa753582(v=vs.85)).

## Project structure

The project structure is inspired by Medium article
[Standard Package Layout](https://medium.com/@benbjohnson/standard-package-layout-7cdbc8391fc1).

## Export from Wallabag

Select the list you want to export (choose "All entries" to export everything),
click the "Export" icon in the top right corner, and then select "JSON".

## Convert to Netscape Bookmark File Format

### Using pre-built executable

Download one of the
[pre-built executables](https://gitlab.com/fleskesvor/wallabag2pocket/-/jobs/artifacts/main/browse?job=build)
for your platform.

Convert your exported article file by running the executable:
```
[executable] -i articles.json -e bookmarks.xml
```

Example on Linux:
```
./w2p-linux -i articles.json -e bookmarks.xml
```

**NOTE:** Executables for Windows and Mac have not been tested.
Feedback appreciated.

### Using go command

Convert your exported article file by running the go command:
```
go run cmd/cli/main.go -i articles.json -e bookmarks.xml
```

## Import into Pocket

Import the converted article export into Pocket using the
[bookmark import tool](https://getpocket.com/import/browser).

## Known issues

- In my testing with 2048 exported articles, the import has failed with a
`504 ERROR` twice, importing "only" 2012 bookmarks both times. I plan
to check if this issue can be resolved by splitting bookmarks into
multiple files.